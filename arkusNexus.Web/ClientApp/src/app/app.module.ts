import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { InputSwitchModule } from 'primeng/inputswitch';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';

import { MainService } from './services/main.service';
import { LocalStoreManager } from './services/local-store-manager.service';
import { MenuComponent } from './components/menu/menu.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { AuthGuard } from './services/auth.guard';
import { MisRecibosComponent } from './components/mis-recibos/mis-recibos.component';
import { MonthTransform } from './pipes/month-transform.pipe';
import { GenerarRecibosComponent } from './components/generar-recibos/generar-recibos.component';

@NgModule({
  declarations: [
    AppComponent,    
    HomeComponent,
    MenuComponent,
    EmpleadosComponent,
    MisRecibosComponent,
    GenerarRecibosComponent,
    MonthTransform
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full', outlet: 'deslogueado' },
      { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },
      { path: 'empleados', component: EmpleadosComponent, canActivate: [AuthGuard] },
      { path: 'miNomina', component: MisRecibosComponent, canActivate: [AuthGuard] },
      { path: 'generarRecibos', component: GenerarRecibosComponent, canActivate: [AuthGuard] },  
    ]),
    InputSwitchModule,
    TableModule,
    ToastModule,
    BrowserAnimationsModule,
    CalendarModule,
  ],
  providers: [
    MainService,
    LocalStoreManager,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
