import { Pipe, PipeTransform } from '@angular/core';
/*
 *  Strip non-numeric characters from a string
 */
@Pipe({ name: 'monthTransform' })
export class MonthTransform implements PipeTransform {
  transform(value: string) {

    var date = new Date(value);

    switch (date.getMonth()+1) {
      case 1:
        return 'Enero ' + date.getFullYear();
      case 2:
        return 'Febrero ' + date.getFullYear();
      case 3:
        return 'Marzo ' + date.getFullYear();
      case 4:
        return 'Abril ' + date.getFullYear();
      case 5:
        return 'Mayo ' + date.getFullYear();
      case 6:
        return 'Junio ' + date.getFullYear();
      case 7:
        return 'Julio ' + date.getFullYear();
      case 8:
        return 'Agosto ' + date.getFullYear();
      case 9:
        return 'Septiembre ' + date.getFullYear();
      case 10:
        return 'Octubre ' + date.getFullYear();
      case 11:
        return 'Noviembre ' + date.getFullYear();
      case 12:
        return 'Diciembre ' + date.getFullYear();
      default:
        return value;
    }
  }
}
