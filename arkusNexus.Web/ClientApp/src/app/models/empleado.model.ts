export class Empleado {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }  
  
  public fi_IdEmpleado: number;
  public fc_Nombre: string;
  public fc_ApellidoPaterno: string;
  public fc_ApellidoMaterno: string;
  public fd_FechaIngreso: Date;
  public fn_IngresoBase: number;
  public fn_DeduccionDesayuno: number;
  public fn_DeduccionAhorro: number;
  public fb_Activo: boolean = true;
  public fi_Role: number;
  public fc_Email: string;
  public fc_Password: string;
  public fc_ConfirmPassword: string;
  public isForBulkDelete: boolean = false;
}
