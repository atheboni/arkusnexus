export class ReciboNomina {
  // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
  constructor() {

  }

  public fi_IdReciboNomina: number;
  public Empleado: string;
  public fd_FechaPeriodo: Date;
  public fn_IngresoBase: number;
  public fn_Prestamos: number;
  public fn_TotalPercepciones: number;
  public fn_Ahorro: number;
  public fn_Desayunos: number;
  public fn_Gasolina: number;
  public fn_TotalDeducciones: number;
  public fn_TotalGeneral: number;
  public fd_FechaGeneracion: Date;
}
