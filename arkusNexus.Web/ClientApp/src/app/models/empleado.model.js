"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Empleado = /** @class */ (function () {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    function Empleado() {
        this.fb_Activo = true;
        this.isForBulkDelete = false;
    }
    return Empleado;
}());
exports.Empleado = Empleado;
//# sourceMappingURL=empleado.model.js.map