import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Empleado } from '../../models/empleado.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AdminEndpoint } from '../../services/admin-endpoint.service';
import { EndpointFactory } from '../../services/endpoint-factory.service';
import { AdminService } from '../../services/admin.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css'],
  providers: [
    EndpointFactory,
    AdminEndpoint,
    AdminService,
    MessageService
  ]
})
/** empleados component*/
export class EmpleadosComponent implements AfterViewChecked, OnInit {
  empleadosArray: Empleado[];
  empleadoSelected: Empleado = new Empleado();
  isEdit: boolean = false;
  empleadosForBulkDelete: number[] = [];
  empleadoForm: FormGroup = new FormGroup({});
  messageIcon: string = "";
  selectedEmpleados: Empleado[];

  acceptConfirm: any;

  cols: any[] = [
    { field: 'fc_Nombre', header: 'Nombre', class: 'gridName' },
    { field: 'fc_ApellidoPaterno', header: 'Apellido Paterno', class: 'gridName' },
    { field: 'fc_ApellidoMaterno', header: 'Apellido Materno', class: 'gridName' },
  ];

  /** Empleados ctor */
  constructor(private adminService: AdminService, private messageService: MessageService) {

  }

  ngOnInit(): void {
    this.loadAllEmpleados();

    this.empleadoForm = new FormGroup({
      fc_Nombre: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      fc_ApellidoPaterno: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      fc_ApellidoMaterno: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      fd_FechaIngreso: new FormControl('', [Validators.required]),
      fn_IngresoBase: new FormControl('', [Validators.required, Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(100)]),
      fn_DeduccionDesayuno: new FormControl('', [Validators.required, Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(100)]),
      fn_DeduccionAhorro: new FormControl('', [Validators.required, Validators.pattern("^[0-9\.]*$"), Validators.min(0), Validators.minLength(1), Validators.maxLength(100)]),
      fi_Role: new FormControl('', [Validators.required, Validators.min(1)]),
      fc_Email: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
      fc_Password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      fc_ConfirmPassword: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
    });
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  loadAllEmpleados() {
    this.adminService.getEmpleados().subscribe((empleados: Empleado[]) => {
      this.onAllEmpleadosLoadSuccessful(empleados);
    }, error => {
      this.showError(error);
    });
  }

  onAllEmpleadosLoadSuccessful(empleados: Empleado[]) {
    this.empleadosArray = empleados;
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  newEmpleado() {
    this.empleadoSelected = new Empleado();
    this.isEdit = true;
  }

  validateExistEmpleado(empleado: Empleado) {
    if (empleado.fc_Password != empleado.fc_ConfirmPassword) {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'Los Passwords no coinciden'
      });
    }
    else if (typeof this.empleadosArray.find(x => x.fc_Email.toLowerCase() == empleado.fc_Email.toLowerCase() && x.fi_IdEmpleado != empleado.fi_IdEmpleado) !== 'undefined') {
      this.messageIcon = 'pi-exclamation-triangle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'error',
        summary: 'Error',
        detail: 'El Empleado ya existe'
      });
    }
    else {
      this.validateExistDisabledEmpleado(empleado);
    }
  }

  validateExistDisabledEmpleado(empleado: Empleado) {
    this.adminService.getExistEmpleado(empleado.fc_Email).subscribe((existentEmpleado: Empleado[]) => {
      if (existentEmpleado.length > 0) {
        this.confirmDuplicateEmpleado(existentEmpleado[0]);
      }
      else {
        this.saveEmpleado(empleado);
      }
    }, error => {
      this.showError(error);
    });
  }

  enableEmpleado() {
    this.empleadoSelected.fb_Activo = true;
    this.saveEmpleado(this.empleadoSelected);

    this.rejectConfirm();
  }

  confirmDuplicateEmpleado(empleado: Empleado) {
    this.empleadoSelected = empleado;
    this.acceptConfirm = this.enableEmpleado;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: 'El Empleado ya existe, pero está deshabilitado',
      detail: '¿Deseas Habilitarlo?',

    });
  }

  saveEmpleado(empleado: Empleado) {
    this.adminService.saveEmpleado(empleado).subscribe((courseResult: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Tus Cambios se han guardado con Éxito'
      });

      this.loadAllEmpleados();
      this.empleadoForm.reset();
      this.isEdit = false;
    }, error => {
      this.showError(error);
    });
  }

  getEmpleadoDetail(empleado: Empleado) {
    this.empleadoSelected = Object.assign({}, empleado);
    this.empleadoSelected.fd_FechaIngreso = new Date(this.empleadoSelected.fd_FechaIngreso);
    this.isEdit = true;
  }

  disableEmpleado() {
    this.adminService.getDeleteEmpleado(this.empleadoSelected.fi_IdEmpleado).subscribe(results => {
      var index = -1;
      index = this.empleadosArray.indexOf(this.empleadoSelected);
      if (index > -1) {
        this.empleadosArray.splice(index, 1);
      }

      this.rejectConfirm();
    }, error => {
      this.showError(error);
    });
  }

  deleteEmpleado(empleado: Empleado) {

    this.empleadoSelected = empleado;
    this.acceptConfirm = this.disableEmpleado;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Empleado',

    });
  }

  addTobulk(checkedEmpleado: Empleado) {
    if (!checkedEmpleado.isForBulkDelete) {
      checkedEmpleado.isForBulkDelete = true;
      this.empleadosForBulkDelete.push(checkedEmpleado.fi_IdEmpleado);
    } else {
      checkedEmpleado.isForBulkDelete = false;
      var index = -1;
      index = this.empleadosForBulkDelete.indexOf(checkedEmpleado.fi_IdEmpleado);
      if (index > -1) {
        this.empleadosForBulkDelete.splice(index, 1);
      }
    }
  }

  bulkDelete() {
    this.acceptConfirm = this.deleteBulk;

    this.messageService.add({
      key: 'confirmation',
      sticky: true,
      severity: 'warn',
      summary: '¿Estas seguro?',
      detail: 'Eliminar Empleados',

    });
  }

  deleteBulk() {
    if (this.empleadosForBulkDelete.length > 0) {
      this.adminService.getBulkDeleteEmpleado(this.empleadosForBulkDelete).subscribe(results => {
        for (var i = 0; i < this.empleadosForBulkDelete.length; i++) {
          var index = -1;
          index = this.empleadosArray.indexOf(this.empleadosArray.find(x => x.fi_IdEmpleado == this.empleadosForBulkDelete[i]));
          if (index > -1) {
            this.empleadosArray.splice(index, 1);
          }
        }
        this.bulkDeleteCompleted();
      }, error => {
        this.showError(error);
      });
    }
  }

  bulkDeleteCompleted() {
    this.empleadosForBulkDelete = [];
    this.rejectConfirm();
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
