import { Component, AfterViewChecked } from '@angular/core';
import { AdminEndpoint } from '../../services/admin-endpoint.service';
import { EndpointFactory } from '../../services/endpoint-factory.service';
import { AdminService } from '../../services/admin.service';
import { MessageService } from 'primeng/api';
import { GenerarRecibos } from '../../models/generarRecibos.model';

@Component({
  selector: 'app-generar-recibos',
  templateUrl: './generar-recibos.component.html',
  styleUrls: ['./generar-recibos.component.css'],
  providers: [
    EndpointFactory,
    AdminEndpoint,
    AdminService,
    MessageService
  ]
})
/** generarRecibos component*/
export class GenerarRecibosComponent implements AfterViewChecked {
  currentYear: number = new Date().getFullYear();
  yearRangeDesde: string = (this.currentYear - 1) + ":" + (this.currentYear + 1);
  fechaPeriodo: Date = new Date(Date.now());
  generarRecibos: GenerarRecibos = { fd_FechaPeriodo: new Date((new Date(Date.now()).getMonth()+1) + '/1/' + new Date(Date.now()).getFullYear()) };
  messageIcon: string = "";

  /** generarRecibos ctor */
  constructor(private adminService: AdminService, private messageService: MessageService) {

  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  genarateRecibos() {
    this.adminService.getCreateRecibos(this.generarRecibos).subscribe((result: number) => {
      this.messageIcon = 'pi-check-circle';
      this.messageService.add({
        key: 'aceptar',
        sticky: true,
        severity: 'success',
        summary: 'Éxito',
        detail: 'Los Recibos se han generado con Éxito'
      });
    }, error => {
      this.showError(error);
    });
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  closeError() {
    this.messageService.clear('aceptar');
  }
}
