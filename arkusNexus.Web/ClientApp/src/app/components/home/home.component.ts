import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { Router } from '@angular/router';

//import { ConfiguracionEndpoint } from '../../services/configuracion-endpoint.service';
import { EndpointFactory } from '../../services/endpoint-factory.service';
import { MessageService } from 'primeng/api';
import { Empleado } from '../../models/empleado.model';
import { Login } from '../../models/login.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminEndpoint } from '../../services/admin-endpoint.service';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    EndpointFactory,
    AdminEndpoint,
    AdminService,
    MessageService
  ]
})
export class HomeComponent implements OnInit {
  loginModel: Login = new Login();
  showLoginError: boolean = false;
  loginForm: FormGroup = new FormGroup({});
  empleadoLogueado: Empleado;

  constructor(private mainService: MainService, private router: Router, private adminService: AdminService) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      password: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
    });
  }

  login() {
    this.adminService.login(this.loginModel).subscribe((empleado: Empleado[]) => {
      this.onLoginSuccessful(empleado);
    }, error => {
      this.showError(error);
    });
  }

  onLoginSuccessful(empleado: Empleado[]) {
    if (empleado.length > 0) {
      this.empleadoLogueado = empleado[0];
      this.mainService.login(this.empleadoLogueado);
      this.router.navigate(['menu']);
      this.showLoginError = false;
    }
    else {
      this.showLoginError = true;
    }
  }  

  showError(error: any) {
    //this.messageIcon = 'pi-exclamation-triangle';
    //this.messageService.add({
    //  key: 'aceptar',
    //  sticky: true,
    //  severity: 'error',
    //  summary: 'Error',
    //  detail: error
    //});
  }
}
