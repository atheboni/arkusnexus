import { Component, OnInit, AfterViewChecked } from '@angular/core';

import { Empleado } from '../../models/empleado.model';
import { ReciboNomina } from '../../models/reciboNomina';

import { AdminEndpoint } from '../../services/admin-endpoint.service';
import { EndpointFactory } from '../../services/endpoint-factory.service';
import { AdminService } from '../../services/admin.service';
import { MessageService } from 'primeng/api';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-mis-recibos',
  templateUrl: './mis-recibos.component.html',
  styleUrls: ['./mis-recibos.component.css'],
  providers: [
    EndpointFactory,
    AdminEndpoint,
    AdminService,
    MessageService
  ]
})
/** misRecibos component*/
export class MisRecibosComponent implements AfterViewChecked, OnInit {
  reciboNominasArray: ReciboNomina[];
  reciboNominaSelected: ReciboNomina = new ReciboNomina();
  isEdit: boolean = false;  
  messageIcon: string = "";  
  acceptConfirm: any;

  cols: any[] = [
    { field: 'fd_FechaPeriodo', header: 'Periodo', class: 'gridName' },    
  ];

  /** misRecibos ctor */
  constructor(private adminService: AdminService, private messageService: MessageService, private mainService: MainService) {

  }

  ngOnInit(): void {
    this.loadAllReciboNominas();    
  }

  ngAfterViewChecked(): void {
    if (document.querySelector('script[src="../../../assets/scripts/hoe.js"]') == null) {
      const node = document.createElement('script');
      node.src = '../../../assets/scripts/hoe.js';
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  loadAllReciboNominas() {
    this.adminService.getRecibosEmpleado(this.mainService.idEmpleado).subscribe((reciboNominas: ReciboNomina[]) => {
      this.onAllReciboNominasLoadSuccessful(reciboNominas);
    }, error => {
      this.showError(error);
    });
  }

  onAllReciboNominasLoadSuccessful(reciboNominas: ReciboNomina[]) {

    this.reciboNominasArray = reciboNominas;
    console.log(this.reciboNominasArray);
  }

  showError(error: any) {
    this.messageIcon = 'pi-exclamation-triangle';
    this.messageService.add({
      key: 'aceptar',
      sticky: true,
      severity: 'error',
      summary: 'Error',
      detail: error
    });
  }

  getReciboNominaDetail(reciboNomina: ReciboNomina) {
    this.reciboNominaSelected = Object.assign({}, reciboNomina);
    this.isEdit = true;
  }

  closeError() {
    this.messageService.clear('aceptar');
  }

  rejectConfirm() {
    this.messageService.clear('confirmation');
  }
}
