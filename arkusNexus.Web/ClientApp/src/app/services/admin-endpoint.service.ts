import { Injectable } from '@angular/core';
import { EndpointFactory } from './endpoint-factory.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Empleado } from '../models/empleado.model';
import { Login } from '../models/login.model';
import { GenerarRecibos } from '../models/generarRecibos.model';

@Injectable()
export class AdminEndpoint extends EndpointFactory {
  private readonly _getLoginUrl: string = "/api/General/Login";

  private readonly _getEmpleadosUrl: string = "/api/Admin/GetEmpleados";
  private readonly _getEmpleadoUrl: string = "/api/Admin/GetEmpleado";
  private readonly _getEmpleadoExistUrl: string = "/api/Admin/GetEmpleadoExist";
  private readonly _saveEmpleadoUrl: string = "/api/Admin/SaveEmpleado";
  private readonly _deleteEmpleadoUrl: string = "/api/Admin/DeleteEmpleado";
  private readonly _bulkDeleteEmpleadoUrl: string = "/api/Admin/BulkDeleteEmpleado";

  private readonly _getRecibosEmpleadoUrl: string = "/api/Admin/GetReciboNominaEmpleado";
  private readonly _getCreateRecibosUrl: string = "/api/Admin/CreateRecibosNomina";

  get getLoginUrl() { return environment.baseUrl + this._getLoginUrl; }

  get getEmpleadosUrl() { return environment.baseUrl + this._getEmpleadosUrl; }
  get getEmpleadoUrl() { return environment.baseUrl + this._getEmpleadoUrl; }
  get getEmpleadoExistUrl() { return environment.baseUrl + this._getEmpleadoExistUrl; }
  get saveEmpleadoUrl() { return environment.baseUrl + this._saveEmpleadoUrl; }
  get deleteEmpleadoUrl() { return environment.baseUrl + this._deleteEmpleadoUrl; }
  get bulkDeleteEmpleadoUrl() { return environment.baseUrl + this._bulkDeleteEmpleadoUrl; }

  get getRecibosEmpleadoUrl() { return environment.baseUrl + this._getRecibosEmpleadoUrl; }
  get getCreateRecibosUrl() { return environment.baseUrl + this._getCreateRecibosUrl; }

  constructor(http: HttpClient) {
    super(http);
  }

  /***************************************************************************************
                                  LOGIN ENDPOINT
    ***************************************************************************************/
  postLoginEndpoint<T>(loginInfo: Login): Observable<T> {
    let endpointUrl = this.getLoginUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(loginInfo), this.getRequestHeaders());
      //.catch(error => {
      //  return this.handleError(error, () => this.postLoginEndpoint(loginInfo));
      //});
  }

  /***************************************************************************************
                                  EMPLEADO ENDPOINTS
    ***************************************************************************************/
  getEmpleadosEndpoint<T>(): Observable<T> {
    let endpointUrl = this.getEmpleadosUrl;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders());
      /*.catchError(error => {
        return this.handleError(error, () => this.getEmpleadosEndpoint());
      });*/
  }

  getExistEmpleadoEndpoint<T>(empleadoName: string): Observable<T> {

    let endpointUrl = `${this.getEmpleadoExistUrl}/${empleadoName}`;
    return this.http.get<T>(endpointUrl, this.getRequestHeaders());
      //.catch(error => {
      //  return this.handleError(error, () => this.getExistEmpleadoEndpoint(empleadoName));
      //});
  }

  postSaveEmpleadoEndpoint<T>(newEmpleado: Empleado): Observable<T> {
    let endpointUrl = this.saveEmpleadoUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(newEmpleado), this.getRequestHeaders());
      //.catch(error => {
      //  return this.handleError(error, () => this.postSaveEmpleadoEndpoint(newEmpleado));
      //});
  }

  getDeleteEmpleadoEndpoint<T>(empleadoId: number): Observable<T> {
    let endpointUrl = `${this.deleteEmpleadoUrl}/${empleadoId}`;

    return this.http.delete<T>(endpointUrl, this.getRequestHeaders());
      //.catch(error => {
      //  return this.handleError(error, () => this.getDeleteEmpleadoEndpoint(empleadoId));
      //});
  }

  getBulkDeleteEmpleadoEndpoint<T>(empleadoIds: number[]): any {
    let endpointUrl = this.bulkDeleteEmpleadoUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(empleadoIds), this.getRequestHeaders());
      //.catch(error => {
      //  return this.handleError(error, () => this.getBulkDeleteEmpleadoEndpoint(empleadoIds));
      //});
  }

/***************************************************************************************
                              RECIBOS NOMINA ENDPOINTS
***************************************************************************************/
  getRecibosEmpleadoEndpoint<T>(empleadoId: number): Observable<T> {
    let endpointUrl = `${this.getRecibosEmpleadoUrl}/${empleadoId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders());
    //.catch(error => {
    //  return this.handleError(error, () => this.getDeleteEmpleadoEndpoint(empleadoId));
    //});
  }

  getCreateRecibosEndpoint<T>(generarRecibos: GenerarRecibos): Observable<T> {
    let endpointUrl = this.getCreateRecibosUrl;

    return this.http.post<T>(endpointUrl, JSON.stringify(generarRecibos), this.getRequestHeaders());
    //.catch(error => {
    //  return this.handleError(error, () => this.getDeleteEmpleadoEndpoint(empleadoId));
    //});
  }
}
