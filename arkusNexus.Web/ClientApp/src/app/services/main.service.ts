import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LocalStoreManager } from './local-store-manager.service';
import { Empleado } from '../models/empleado.model';
import { Router } from '@angular/router';

@Injectable()
export class MainService {
  constructor(private localStorage: LocalStoreManager, private router: Router) {

  }

  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  // Service message commands
  emitChange(count: any) {
    this.emitChangeSource.next(count);
  }

  login(user?: Empleado) {
    if (user) {
      this.localStorage.saveSyncedSessionData(user, 'current_user');
    }
    
    this.emitChangeSource.next(true);
  }

  logout() {

    var allsuspects = document.getElementsByTagName("script")
    for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
      if (allsuspects[i] && allsuspects[i].getAttribute("src") != null && allsuspects[i].getAttribute("src").indexOf("hoe.js") != -1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }


    this.localStorage.deleteData('current_user');
    this.localStorage.deleteData('configuracion_general');
    this.emitChangeSource.next(false);
  }

  get nombreEmpleado() {
    var empleado = this.localStorage.getDataObject<Empleado>('current_user');
    if (empleado != null) {
      return empleado.fc_Nombre + ' ' + empleado.fc_ApellidoMaterno + ' ' + empleado.fc_ApellidoMaterno;
    }
    else {
      return '';
    }
  }

  //set nombreEmpleado(_nombreEmpleado: string) {
  //  var empleado = this.localStorage.getDataObject<Empleado>('current_user');
  //  if (empleado != null) {
  //    empleado.fc_Nombre = _nombreEmpleado;

  //    this.localStorage.saveSyncedSessionData(empleado, 'current_user');
  //    this.emitChangeSource.next(true);
  //  }
  //}

  get nivelEmpleado() {
    var empleado = this.localStorage.getDataObject<Empleado>('current_user');
    if (empleado != null) {
      return empleado.fi_Role;
    }
    else {
      return 0;
    }
  }

  get idEmpleado() {
    var empleado = this.localStorage.getDataObject<Empleado>('current_user');
    if (empleado != null) {
      return empleado.fi_IdEmpleado;
    }
    else {
      return 0;
    }
  } 
}
