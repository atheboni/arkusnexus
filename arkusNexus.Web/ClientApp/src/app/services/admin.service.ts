import { Injectable } from '@angular/core';
import { AdminEndpoint } from './admin-endpoint.service';
import { Empleado } from '../models/Empleado.model';
import { Login } from '../models/login.model';
import { ReciboNomina } from '../models/reciboNomina';
import { GenerarRecibos } from '../models/generarRecibos.model';

@Injectable()
export class AdminService {
  constructor(private adminEndpoint: AdminEndpoint) {

  }

  /***************************************************************************************
                                 LOGIN ENDPOINT
   ***************************************************************************************/
  login(loginInfo: Login) {
    return this.adminEndpoint.postLoginEndpoint<Empleado[]>(loginInfo);
  }

  /***************************************************************************************
                                  EMPLEADO ENDPOINTS
    ***************************************************************************************/
  getEmpleados() {
    return this.adminEndpoint.getEmpleadosEndpoint<Empleado[]>();
  }

  getExistEmpleado(empleadoName: string) {
    return this.adminEndpoint.getExistEmpleadoEndpoint<Empleado[]>(empleadoName);
  }

  saveEmpleado(newEmpleado: Empleado) {
    return this.adminEndpoint.postSaveEmpleadoEndpoint<number>(newEmpleado);
  }

  getDeleteEmpleado(empleadoId: number) {
    return this.adminEndpoint.getDeleteEmpleadoEndpoint<number>(empleadoId);
  }

  getBulkDeleteEmpleado(empleadoIds: number[]) {
    return this.adminEndpoint.getBulkDeleteEmpleadoEndpoint<number>(empleadoIds);
  }

/***************************************************************************************
                                RECIBOS NOMINA ENDPOINTS
  ***************************************************************************************/
  getRecibosEmpleado(empleadoId: number) {
    return this.adminEndpoint.getRecibosEmpleadoEndpoint<ReciboNomina[]>(empleadoId);
  }

  getCreateRecibos(generarRecibos: GenerarRecibos) {
    return this.adminEndpoint.getCreateRecibosEndpoint<number>(generarRecibos);
  }
}
