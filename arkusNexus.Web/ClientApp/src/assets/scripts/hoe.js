$(document).ready(function () {
    HoeDatapp = {
        appinit: function () {
            HoeDatapp.HandleSidebartoggle();
            HoeDatapp.Handlelpanel();
            HoeDatapp.Handlelpanelmenu();
            HoeDatapp.Handlethemeoption();
            HoeDatapp.Handlesidebareffect();
            HoeDatapp.Handlesidebarposition();
            HoeDatapp.Handlecontentheight();
            HoeDatapp.Handlethemecolor();
            HoeDatapp.Handlenavigationtype();
            HoeDatapp.Handlesidebarside();
            HoeDatapp.Handleactivestatemenu();
            HoeDatapp.Handlethemelayout();
            HoeDatapp.Handlethemebackground()
        },
        Handlethemebackground: function () {
            (function () {
                $('#theme-color > a.theme-bg').on('click', function () {
                    $('body').attr('theme-bg', $(this).attr('hoe-themebg-type'))
                })
            })()
        },
        Handlethemelayout: function () {
            $('#theme-layout').on('change', function () {
                if ($(this).val() == 'box-layout') $('body').attr('theme-layout', 'box-layout');
                else $('body').attr('theme-layout', 'wide-layout')
            })
        },
        Handleactivestatemenu: function () {
            $('.panel-list li > a').on('click', function () {
                $('body').attr('hoe-navigation-type') != 'vertical' && $('body').attr('hoe-navigation-type') != 'vertical-compact' || 0 !== $(this).parent('li.hoe-has-menu').length || ($(this).parents('.panel-list').find('li.active').removeClass('active'), $(this).parent().addClass('active'))
            })
        },
        Handlesidebarside: function () {
            $('#navigation-side').on('change', function () {
                if ($(this).val() == 'rightside') $('body').attr('hoe-nav-placement', 'right');
                else $('body').attr('hoe-nav-placement', 'left');
                $('body').attr('hoe-navigation-type', 'vertical');
                $('#hoeapp-wrapper').removeClass('compact-hmenu')
            })
        },
        Handlenavigationtype: function () {
            $('#navigation-type').on('change', function () {
                $(this).val() == 'horizontal' ? ($('body').attr('hoe-navigation-type', 'horizontal'), $('#hoeapp-wrapper').removeClass('compact-hmenu'), $('#hoe-header, #hoeapp-container').removeClass('hoe-minimized-lpanel'), $('body').attr('hoe-nav-placement', 'left'), $('#hoe-header').attr('hoe-color-type', 'logo-bg7')) : $(this).val() == 'horizontal-compact' ? ($('body').attr('hoe-navigation-type', 'horizontal'), $('#hoeapp-wrapper').addClass('compact-hmenu'), $('#hoe-header, #hoeapp-container').removeClass('hoe-minimized-lpanel'), $('body').attr('hoe-nav-placement', 'left'), $('#hoe-header').attr('hoe-color-type', 'logo-bg7')) : ($(this).val() == 'vertical-compact' ? ($('body').attr('hoe-navigation-type', 'vertical-compact'), $('#hoeapp-wrapper').removeClass('compact-hmenu'), $('#hoe-header, #hoeapp-container').addClass('hoe-minimized-lpanel')) : ($('body').attr('hoe-navigation-type', 'vertical'), $('#hoeapp-wrapper').removeClass('compact-hmenu'), $('#hoe-header, #hoeapp-container').removeClass('hoe-minimized-lpanel')), $('body').attr('hoe-nav-placement', 'left'))
            })
        },
        Handlethemecolor: function () {
            (function () {
                $('#theme-color > a.header-bg').on('click', function () {
                    $('#hoe-header > .hoe-right-header').attr('hoe-color-type', $(this).attr('hoe-color-type'))
                })
            })();
            (function () {
                $('#theme-color > a.lpanel-bg').on('click', function () {
                    $('#hoeapp-container').attr('hoe-color-type', $(this).attr('hoe-color-type'))
                })
            })();
            (function () {
                $('#theme-color > a.logo-bg').on('click', function () {
                    $('#hoe-header').attr('hoe-color-type', $(this).attr('hoe-color-type'))
                })
            })()
        },
        Handlecontentheight: function () {
            function a() {
                var a = $(window).height(),
                    c = $('#hoe-header').innerHeight(),
                    //b = $('#footer').innerHeight(),
                    b = 10,
                    b = a - c - b - 2,
                    a = a - c - 2;
                $('#main-content ').css('min-height', b);
                $('.inner-left-panel ').css('height', a)
            }
            a();
            $(window).resize(function () {
                a();
            })
        },
        Handlesidebarposition: function () {
            $('#sidebar-position').on('change', function () {
                if ($(this).val() == 'fixed') $('#hoe-left-panel,.hoe-left-header').attr('hoe-position-type', 'fixed');
                else $('#hoe-left-panel,.hoe-left-header').attr('hoe-position-type', 'absolute')
            })
        },
        Handlesidebareffect: function () {
            $('#leftpanel-effect').on('change', function () {
                if ($(this).val() == 'overlay') $('#hoe-header, #hoeapp-container').attr('hoe-lpanel-effect', 'overlay');
                else if ($(this).val() == 'push') $('#hoe-header, #hoeapp-container').attr('hoe-lpanel-effect', 'push');
                else $('#hoe-header, #hoeapp-container').attr('hoe-lpanel-effect', 'shrink')
            })
        },
        Handlethemeoption: function () {
            $('.selector-toggle > a').on('click', function () {
                $('#styleSelector').toggleClass('open');
            })
        },
        Handlelpanelmenu: function () {
            $('.hoe-has-menu > a').on('click', function () {
                if (0 === $(this).closest('.hoe-minimized-lpanel').length) {
                    $(this).parent('.hoe-has-menu').parent('ul').find('ul:visible').slideUp('fast');
                    $(this).parent('.hoe-has-menu').parent('ul').find('.opened').removeClass('opened');
                    var a = $(this).parent('.hoe-has-menu').find('>.hoe-sub-menu');
                    a.is(':hidden') ? (a.slideDown('fast'), $(this).parent('.hoe-has-menu').addClass('opened')) : ($(this).parent('.hoe-has-menu').parent('ul').find('ul:visible').slideUp('fast'), $(this).parent('.hoe-has-menu').removeClass('opened'))
                }
            })
        },
        HandleSidebartoggle: function () {
            $('.hoe-sidebar-toggle a').on('click', function () {
                if ($('#hoeapp-wrapper').attr('hoe-device-type') !== 'phone')
                    if ($('#hoeapp-container').toggleClass('hoe-minimized-lpanel'), $('#hoe-header').toggleClass('hoe-minimized-lpanel'), $('body').attr('hoe-navigation-type') !== 'vertical-compact') $('body').attr('hoe-navigation-type', 'vertical-compact');
                    else $('body').attr('hoe-navigation-type', 'vertical');
                else if ($('#hoeapp-wrapper').hasClass('hoe-hide-lpanel')) $('#hoeapp-wrapper').removeClass('hoe-hide-lpanel');
                else $('#hoeapp-wrapper').addClass('hoe-hide-lpanel')
            })
        },
        Handlelpanel: function () {
            function a() {
                var a = $(window)[0].innerWidth;
                768 <= a && 1024 >= a ? ($('#hoeapp-wrapper').attr('hoe-device-type', 'tablet'), $('#hoe-header, #hoeapp-container').addClass('hoe-minimized-lpanel'), $('li.theme-option select').attr('disabled', false)) : 768 > a ? ($('#hoeapp-wrapper').attr('hoe-device-type', 'phone'), $('#hoe-header, #hoeapp-container').removeClass('hoe-minimized-lpanel'), $('li.theme-option select').attr('disabled', 'disabled')) : ($('body').attr('hoe-navigation-type') !== 'vertical-compact' ? ($('#hoeapp-wrapper').attr('hoe-device-type', 'desktop'), $('#hoe-header, #hoeapp-container').removeClass('hoe-minimized-lpanel')) : ($('#hoeapp-wrapper').attr('hoe-device-type', 'desktop'), $('#hoe-header, #hoeapp-container').addClass('hoe-minimized-lpanel')), $('li.theme-option select').attr('disabled', false))
            }
            a();
            $(window).resize(a)
        }
    };
    HoeDatapp.appinit()
});
