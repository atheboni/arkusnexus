﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arkusNexus.WebApi.ViewModels
{
    public class ReciboNominaViewModel
    {
        public int fi_IdReciboNomina { get; set; }
        public string Empleado { get; set; }
        public DateTime fd_FechaPeriodo { get; set; }
        public decimal fn_IngresoBase { get; set; }
        public decimal fn_Prestamos { get; set; }
        public decimal fn_TotalPercepciones { get; set; }
        public decimal fn_Ahorro { get; set; }
        public decimal fn_Desayunos { get; set; }
        public decimal fn_Gasolina { get; set; }
        public decimal fn_TotalDeducciones { get; set; }
        public decimal fn_TotalGeneral { get; set; }
        public DateTime fd_FechaGeneracion { get; set; }
    }
}
