﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arkusNexus.WebApi.ViewModels
{
    public class EmpleadoViewModel
    {
        public int fi_IdEmpleado { get; set; }
        public string fc_Nombre { get; set; }
        public string fc_ApellidoPaterno { get; set; }
        public string fc_ApellidoMaterno { get; set; }
        public DateTime fd_FechaIngreso { get; set; }
        public decimal fn_IngresoBase { get; set; }
        public decimal fn_DeduccionDesayuno { get; set; }
        public decimal fn_DeduccionAhorro { get; set; }
        public bool fb_Activo { get; set; }
        public int fi_Role { get; set; }
        public string fc_Email { get; set; }
        public string fc_Password { get; set; }
        public string fc_ConfirmPassword { get; set; }
    }
}
