﻿using arkusNexus.WebApi.Models.Admin;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arkusNexus.WebApi.ViewModels
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Empleado, EmpleadoViewModel>()
                   .ForMember(d => d.fc_ConfirmPassword, map => map.Ignore())
                   .ReverseMap();

            CreateMap<ReciboNomina, ReciboNominaViewModel>()
                   .ForMember(d => d.Empleado, map => map.MapFrom(s => s.Empleado.fc_Nombre+' '+s.Empleado.fc_ApellidoPaterno+' '+s.Empleado.fc_ApellidoMaterno))
                   .ReverseMap();
        }
    }
}
