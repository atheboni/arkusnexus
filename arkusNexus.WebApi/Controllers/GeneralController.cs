﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using arkusNexus.WebApi.Models;
using arkusNexus.WebApi.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace arkusNexus.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GeneralController : ControllerBase
    {
        private readonly arkusNexusDbContext _context;
        private IHostingEnvironment _hostingEnvironment;

        public GeneralController(arkusNexusDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Login
        [HttpPost]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {
            try
            {
                var usuario = _context.Empleado.Where(m => m.fc_Email.ToUpper() == loginViewModel.email.ToUpper() && m.fc_Password == loginViewModel.password);
                var result = Mapper.Map<IEnumerable<EmpleadoViewModel>>(usuario);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for User from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion
    }
}