﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using arkusNexus.WebApi.Models;
using arkusNexus.WebApi.Models.Admin;
using arkusNexus.WebApi.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace arkusNexus.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly arkusNexusDbContext _context;
        private IHostingEnvironment _hostingEnvironment;

        public AdminController(arkusNexusDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Empleado       
        [HttpGet]
        public IActionResult GetEmpleados()
        {
            try
            {
                var Empleados = _context.Empleado.Where(m => m.fb_Activo).OrderBy(m => m.fc_Nombre).ThenBy(m => m.fc_ApellidoPaterno).ThenBy(m => m.fc_ApellidoMaterno);
                var result = Mapper.Map<IEnumerable<EmpleadoViewModel>>(Empleados);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Empleados from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{EmpleadoId}")]
        public IActionResult GetEmpleado(int EmpleadoId)
        {
            try
            {
                var Empleado = _context.Empleado.Find(EmpleadoId);
                var result = Mapper.Map<EmpleadoViewModel>(Empleado);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Empleado from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpGet("{EmpleadoEmail}")]
        public IActionResult GetEmpleadoExist(string EmpleadoEmail)
        {
            try
            {
                var Empleado = _context.Empleado.Where(m => m.fc_Email.ToUpper() == EmpleadoEmail.ToUpper() && !m.fb_Activo);
                var result = Mapper.Map<IEnumerable<EmpleadoViewModel>>(Empleado);
                return Ok(result);

            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting Empleado from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult SaveEmpleado([FromBody]Empleado newEmpleado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (newEmpleado == null)
                        return BadRequest($"{nameof(newEmpleado)} cannot be null");

                    if (newEmpleado.fi_IdEmpleado == 0)
                        _context.Empleado.Add(newEmpleado);
                    else
                        _context.Empleado.Update(newEmpleado);

                    var result = _context.SaveChanges();
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed saving Empleado: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{EmpleadoId}")]
        public IActionResult DeleteEmpleado(int EmpleadoId)
        {
            try
            {
                Empleado deletedEmpleado = _context.Empleado.Find(EmpleadoId);
                deletedEmpleado.fb_Activo = false;
                _context.Empleado.Update(deletedEmpleado);
                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Empleado from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPut]
        public IActionResult BulkDeleteEmpleado([FromBody]int[] deletedEmpleados)
        {
            try
            {
                foreach (int deletedEmpleadoId in deletedEmpleados)
                {
                    Empleado deletedEmpleado = _context.Empleado.Find(deletedEmpleadoId);
                    deletedEmpleado.fb_Activo = false;
                    _context.Empleado.Update(deletedEmpleado);
                }

                int result = _context.SaveChanges();
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed deleting Empleados from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }
        #endregion

        #region RecibosNomina
        [HttpGet("{EmpleadoId}")]
        public IActionResult GetReciboNominaEmpleado(int EmpleadoId)
        {
            try
            {
                var recibos = _context.ReciboNomina
                    .Include(m => m.Empleado)
                    .Where(m => m.Empleado.fi_IdEmpleado == EmpleadoId)
                    .OrderByDescending(m => m.fd_FechaPeriodo);

                var result = Mapper.Map<IEnumerable<ReciboNominaViewModel>>(recibos);
                return Ok(result);
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed getting data for Recibos Nomina from data store: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }
        }

        [HttpPost]
        public IActionResult CreateRecibosNomina([FromBody]GenerarRecibosViewModel generarRecibos)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (generarRecibos == null)
                        return BadRequest($"{nameof(generarRecibos)} cannot be null");
                   
                    var result = _context.Database.ExecuteSqlCommand("Admin.uspCreateRecibosNomina @p0", generarRecibos.fd_FechaPeriodo);
                    return Ok(result);
                }
            }
            catch (Exception GetCourseExc)
            {
                string message = $"Failed creating Recibos Nomina: Error={GetCourseExc.Message}";
                return StatusCode(404, message);
            }

            return BadRequest(ModelState);
        }
        #endregion
    }
}