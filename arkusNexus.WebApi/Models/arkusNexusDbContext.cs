﻿using arkusNexus.WebApi.Models.Admin;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace arkusNexus.WebApi.Models
{
    public class arkusNexusDbContext : DbContext
    {
        public arkusNexusDbContext(DbContextOptions<arkusNexusDbContext> options)
            : base(options)
        {
        }

        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<ReciboNomina> ReciboNomina { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(local);Database=arkusNexus;User Id=sa;Password=dbmaster*123;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Administrativo
            modelBuilder.Entity<Empleado>(builder =>
            {
                builder.ToTable($"tbl_CatEmpleados", $"Admin");

                builder.HasKey(e => e.fi_IdEmpleado);

                builder.HasIndex(e => e.fi_IdEmpleado)
                    .IsUnique();

                builder.Property(e => e.fc_Nombre)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(e => e.fc_ApellidoPaterno)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(e => e.fc_ApellidoMaterno)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(e => e.fd_FechaIngreso)
               .HasColumnType("datetime")
               .IsRequired();

                builder.Property(e => e.fn_IngresoBase)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(e => e.fn_DeduccionDesayuno)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(e => e.fn_DeduccionAhorro)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(e => e.fb_Activo)
                .IsRequired();

                builder.Property(e => e.fi_Role)
                .IsRequired();

                builder.Property(e => e.fc_Email)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(250);

                builder.Property(e => e.fc_Password)
                .HasColumnType("varchar")
                .IsRequired()
                .HasMaxLength(15);                
            });

            modelBuilder.Entity<ReciboNomina>(builder =>
            {
                builder.ToTable($"tbl_CnfRecibosNomina", $"Admin");

                builder.HasKey(rn => rn.fi_IdReciboNomina);

                builder.HasIndex(rn => rn.fi_IdReciboNomina)
                    .IsUnique();

                builder.HasOne<Empleado>(c => c.Empleado)
                .WithMany()
                .HasForeignKey($"fi_IdEmpleado");

                builder.Property(rn => rn.fd_FechaPeriodo)
               .HasColumnType("datetime")
               .IsRequired();               

                builder.Property(rn => rn.fn_IngresoBase)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_Prestamos)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_TotalPercepciones)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_Ahorro)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_Desayunos)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_Gasolina)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_TotalDeducciones)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fn_TotalGeneral)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

                builder.Property(rn => rn.fd_FechaGeneracion)
               .HasColumnType("datetime")
               .IsRequired();
            });
            #endregion
        }
    }
}
