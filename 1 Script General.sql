IF NOT EXISTS (SELECT 1 FROM sysdatabases WHERE name = 'arkusNexus')
BEGIN
	EXEC ('CREATE DATABASE arkusNexus')
	PRINT 'Se cre� la Base de Datos arkusNexus'
END
GO
BEGIN TRANSACTION ScriptArkusNexus
	USE arkusNexus

	/*SCHEMAS*/	
	IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE name = 'Admin')
	BEGIN
		EXEC ('USE arkusNexus')
		EXEC ('CREATE SCHEMA [Admin]')
		PRINT 'Se cre� el Schema Admin'
	END	

	/*TABLAS*/
	IF OBJECT_ID('Admin.tbl_CatEmpleados') IS NULL
	BEGIN
		CREATE TABLE Admin.tbl_CatEmpleados
		(
			fi_IdEmpleado INT IDENTITY(1,1) NOT NULL,
			fc_Nombre VARCHAR(250) NOT NULL,
			fc_ApellidoPaterno VARCHAR(250) NOT NULL,
			fc_ApellidoMaterno VARCHAR(250) NOT NULL,
			fd_FechaIngreso DATETIME NOT NULL,
			fn_IngresoBase DECIMAL(10,2) NOT NULL,
			fn_DeduccionDesayuno DECIMAL(10,2) NOT NULL,
			fn_DeduccionAhorro DECIMAL(10,2) NOT NULL,
			fb_Activo BIT NOT NULL DEFAULT 1,
			fi_Role INT NOT NULL,			
			fc_Email VARCHAR(250) NOT NULL,
			fc_Password VARCHAR(250) NOT NULL,			
			CONSTRAINT [PK_tbl_CatEmpleados] PRIMARY KEY CLUSTERED(fi_IdEmpleado)
		)
	END	

	IF OBJECT_ID('Admin.tbl_CnfRecibosNomina') IS NULL
	BEGIN
		CREATE TABLE Admin.tbl_CnfRecibosNomina
		(
			fi_IdReciboNomina INT IDENTITY(1,1) NOT NULL,
			fi_IdEmpleado INT NOT NULL,
			fd_FechaPeriodo DATETIME NOT NULL,
			fn_IngresoBase DECIMAL(10,2) NOT NULL,			
			fn_Prestamos DECIMAL(10,2) NOT NULL,
			fn_TotalPercepciones DECIMAL(10,2) NOT NULL,
			fn_Ahorro DECIMAL(10,2) NOT NULL,
			fn_Desayunos DECIMAL(10,2) NOT NULL,
			fn_Gasolina DECIMAL(10,2) NOT NULL,
			fn_TotalDeducciones DECIMAL(10,2) NOT NULL,
			fn_TotalGeneral DECIMAL(10,2) NOT NULL,
			fd_FechaGeneracion DATETIME NOT NULL,
			CONSTRAINT [PK_tbl_CnfRecibosNomina] PRIMARY KEY CLUSTERED(fi_IdReciboNomina)
		)
	END			

	/*LLAVES*/	
	IF NOT EXISTS(SELECT A.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS A WITH(NOLOCK) WHERE A.CONSTRAINT_SCHEMA='Admin' AND A.CONSTRAINT_NAME='FK_CnfRecibosNomina_REF_CatEmpleados')
	BEGIN
		ALTER TABLE Admin.tbl_CnfRecibosNomina
		ADD CONSTRAINT FK_CnfRecibosNomina_REF_CatEmpleados FOREIGN KEY (fi_IdEmpleado) REFERENCES Admin.tbl_CatEmpleados (fi_IdEmpleado)
	END	
		
	/*INFO INICIAL*/
	IF NOT EXISTS(SELECT 1 FROM Admin.tbl_CatEmpleados WHERE fc_Email = 'admin@arkusnexus.com')
	BEGIN
		INSERT INTO Admin.tbl_CatEmpleados 
		(	fc_Nombre,
			fc_ApellidoPaterno,
			fc_ApellidoMaterno,
			fd_FechaIngreso,
			fn_IngresoBase,
			fn_DeduccionDesayuno,
			fn_DeduccionAhorro,
			fb_Activo,
			fi_Role,
			fc_Email,
			fc_Password)
		VALUES ('ADMIN',
				'ARKUS',
				'NEXUS',
				GETDATE(),
				2000.00,
				100.00,
				50.00,
				1,
				1,
				'admin@arkusnexus.com',
				'admin*123')
	END

	/*STORED PROCEDURES*/
	IF OBJECT_ID('Admin.uspCreateRecibosNomina', 'P') IS NOT NULL BEGIN
		DROP PROCEDURE Admin.uspCreateRecibosNomina
	END
	EXECUTE SP_EXECUTESQL N'
	/*******************************************************************************************************************************************
	-- Autor: Bonilla Cruz Alejandro
	-- Descripci�n:	Genera los recibos nominales para el periodo enviado en el par�metro pfd_FechaPeriodo
	-- Fecha de Creaci�n: 23-12-2019

	-- Par�metros de entrada:	
		@pfd_FechaPeriodo					:Contiene la fecha del periodo para el cual se van a generar los recibos de n�mina
	********************************************************************************************************************************************/
	CREATE PROCEDURE Admin.uspCreateRecibosNomina
	(		
		@pfd_FechaPeriodo	DATETIME
	)
	AS
	BEGIN
		SET NOCOUNT ON;		
		
		BEGIN TRY
			DECLARE @tblEmpleadosActivos	TABLE (fi_IdEmpleado INT)
			DECLARE	@idEmpleadoAux			INT

			IF NOT EXISTS(SELECT 1 FROM Admin.tbl_CnfRecibosNomina WHERE fd_FechaPeriodo = @pfd_FechaPeriodo)
			BEGIN		
				INSERT INTO @tblEmpleadosActivos
				SELECT	fi_IdEmpleado 
				FROM	Admin.tbl_CatEmpleados
				WHERE	fb_Activo = 1

				BEGIN TRANSACTION creaRecibos
					WHILE EXISTS(SELECT 1 FROM @tblEmpleadosActivos)
					BEGIN
						SELECT TOP(1) @idEmpleadoAux = fi_IdEmpleado FROM @tblEmpleadosActivos
	
						INSERT INTO Admin.tbl_CnfRecibosNomina (fi_IdEmpleado,
																fd_FechaPeriodo,
																fn_IngresoBase,
																fn_Prestamos,
																fn_TotalPercepciones,
																fn_Ahorro,
																fn_Desayunos,
																fn_Gasolina,
																fn_TotalDeducciones,
																fn_TotalGeneral,
																fd_FechaGeneracion)
						SELECT	fi_IdEmpleado,
								@pfd_FechaPeriodo,
								fn_IngresoBase,
								0,
								(fn_IngresoBase+ 0),
								fn_DeduccionAhorro,
								fn_DeduccionDesayuno,
								0,
								(fn_DeduccionAhorro+fn_DeduccionDesayuno+0),
								(fn_IngresoBase+ 0)-(fn_DeduccionAhorro+fn_DeduccionDesayuno+0),
								GETDATE()
						FROM	Admin.tbl_CatEmpleados WITH(NOLOCK)
						WHERE	fi_IdEmpleado = @idEmpleadoAux

						DELETE FROM @tblEmpleadosActivos WHERE fi_IdEmpleado = @idEmpleadoAux
					END	
				COMMIT TRANSACTION creaRecibos
				SELECT 1
			END
			ELSE
			BEGIN
				SELECT 2
			END
			SET NOCOUNT OFF;
		END TRY
		BEGIN CATCH
  			ROLLBACK TRANSACTION creaRecibos			
		SET NOCOUNT OFF;

		END CATCH			
	END'
COMMIT TRANSACTION ScriptArkusNexus